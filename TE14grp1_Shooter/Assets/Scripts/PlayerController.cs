﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    GameObject projectilePrefab;

    [SerializeField]
    Transform projectileOrigin;

    [SerializeField]
    float timeBetweenShots = 1f;

    float shootCountdown = 1f;

    int points = 0;


    // Update is called once per frame
    void Update()
    {
        

        float speed = 0.1f;

        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");

        Vector3 movementX = new Vector3(speed * moveX, 0, 0);
        Vector3 movementY = new Vector3(0, speed * moveY, 0);

        transform.Translate(movementX + movementY);

        if (transform.position.x > 3 || transform.position.x < -3)
        {
            transform.Translate(-movementX);
        }

        if (transform.position.y > 4.5f || transform.position.y < -4.5f)
        {
            transform.Translate(-movementY);
        }

        shootCountdown -= Time.deltaTime;

        if (Input.GetAxisRaw("Fire1") > 0 && shootCountdown < 0)
        {
            Instantiate(projectilePrefab, projectileOrigin.position, Quaternion.identity);
            shootCountdown = timeBetweenShots;
        }

    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Enemy")
        {
            SceneManager.LoadScene(1);
        }
    }

    public void AddPoints(int p)
    {
        points += p;

        GameObject text = GameObject.FindGameObjectWithTag("Scoretext");

        Text scoreText = text.GetComponent<Text>();

        scoreText.text = points.ToString();
    }
}
