﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AsteroidController : MonoBehaviour {

    [SerializeField]
    float speed = 2f;

    Vector3 movement;

	// Use this for initialization
	void Start () {
        movement = Vector3.down * speed;

        this.transform.position = new Vector3(Random.Range(-4.5f, 4.5f), 5, 0);
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(movement * Time.deltaTime);

        if (transform.position.y < -5)
        {
            Destroy(this.gameObject);
        }

	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Shot")
        {
            Destroy(this.gameObject);

            GameObject ship = GameObject.FindGameObjectWithTag("Player");

            PlayerController pc = ship.GetComponent<PlayerController>();

            pc.AddPoints(100);

        }
    }
}
