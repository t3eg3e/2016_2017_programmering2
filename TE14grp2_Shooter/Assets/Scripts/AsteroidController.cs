﻿using UnityEngine;
using System.Collections;

public class AsteroidController : MonoBehaviour
{
    [SerializeField]
    float speed = 2;

    Vector3 movement;

    // Use this for initialization
    void Start()
    {
        movement = Vector3.down * speed;

        transform.position = new Vector3(Random.Range(-4.5f, 4.5f), 5, 0);

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(movement * Time.deltaTime);

        if (transform.position.y < -5)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "PlayerProjectile")
        {
            Destroy(this.gameObject);
        }
    }

}
