﻿using UnityEngine;
using System.Collections;

public class AsteroidGenerator : MonoBehaviour {

    float timerValue = 0;

    [SerializeField]
    float timeBetweenAsteroids = 2;

    [SerializeField]
    GameObject astroidPrefab;
	
	// Update is called once per frame
	void Update () {

        timerValue += Time.deltaTime;
        if (timerValue > timeBetweenAsteroids)
        {
            timerValue = 0;
            Instantiate(astroidPrefab);
        }

	}
}
